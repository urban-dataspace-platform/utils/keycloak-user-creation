"""
main.py

Description:
    This module serves as the entry point for the project. It orchestrates the execution
    of various tasks by calling functions from other modules based on the project's requirements.
Author:
    Thomas Haarhoff <thomas.haarhoff@hypertegrity.de>

License
    This module is licensed under the European Union Public License, version 1.2.

    EUPL-1.2 License Summary:
    - Copyleft: Modifications and derivative works must be licensed under the same terms.
    - Inherited Obligations: If you create a Derived Work, you must mmake your changes available under the EUPL.

    For the full text of the EUPL-1.2 license, please see the accompanying LICENSE file or visit:
    https://opensource.org/licenses/EUPL-1.2
"""
import sys

from cli import arguments
from processes import update_users
from processes import export_users
from processes import reset_passwords


if __name__ == '__main__':

    # Main entry point for the Keycloak user management script.

    # This script provides functionality for resetting passwords, exporting user statistics, and updating user
    # information in a Keycloak realm.

    # The script reads command-line arguments using the argparse module and delegates tasks to the respective modules:
    # - reset_passwords: Handles password resets for users.
    # - export_users: Prints current user statistics.
    # - update_users: Updates or creates user information in the Keycloak realm.

    # Note:
    # - Make sure to provide the necessary command-line arguments for the desired operation.

    args = arguments.parse_arguments()

    if args.reset_password:
        reset_passwords.reset_passwords(args)
        sys.exit(0)
    elif args.export_only:
        export_users.print_current_user_stats(args)
        sys.exit(0)
    else:
        update_users.update_users(args)
