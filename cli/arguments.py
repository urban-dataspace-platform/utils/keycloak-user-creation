"""
arguments.py

Description:
    This module defines the command-line arguments and options for the project. It utilizes
    the argparse module to parse command-line inputs and provide a configurable interface.

"""
import argparse


def parse_arguments():
    """
    Parse command line arguments for the user management script.

    Returns:
    argparse.Namespace: An object containing parsed command line arguments.

    Raises:
    argparse.ArgumentError: If there is an issue with the command line arguments.

    Note:
    - The function uses the argparse module to define and parse command line arguments.
    - The arguments include options for specifying a user CSV file, default password, admin credentials,
      encoding, domain, Keycloak realm, and additional flags for update, export-only, and password reset.
    - The function returns an argparse.Namespace object containing the parsed arguments.
    """
    parser = argparse.ArgumentParser(
        formatter_class=argparse.ArgumentDefaultsHelpFormatter
    )

# TODO make -c and -ex an exclusive group
    parser.add_argument(
        "-c",
        "--csv",
        required=True,
        dest="user_csv",
        help="Path to user.csv"
    )

    parser.add_argument(
        "-dp",
        "--default_password",
        required=True,
        dest="default_password",
        help="Default Password to be set for the created users"
    )

    parser.add_argument(
        "-a",
        "--admin",
        required=True,
        dest="username",
        help="Admin Username"
    )

    parser.add_argument(
        "-p",
        "--pass",
        required=True,
        dest="password",
        help="Admin Password"
    )

    parser.add_argument(
        "-e",
        "--encoding",
        dest="encoding",
        help="Set Encoding for input file",
        default="UTF-8"
    )

    parser.add_argument(
        "-d",
        "--domain",
        required=True,
        dest="domain",
        help="Platform Domain (i.e. stage.beispiel-stadt.de)"
    )

    parser.add_argument(
        "-r",
        "--realm",
        required=True,
        dest="realm",
        help="Keycloak Realm Name"
    )

    parser.add_argument(
        "-u",
        "--update_existing_users",
        dest="update_existing_users",
        action="store_true",
        help="Update Role if User exists already"
    )

    parser.add_argument(
        "-dl",
        "--delimiter",
        dest="delimiter",
        help="Set Delimiter for roles in the USER_CSV file",
        default="+"
    )

    parser.add_argument(
        "-ex",
        "--export_only",
        dest="export_only",
        action="store_true",
        help="Only generate a list of current users."
    )

    parser.add_argument(
        "-rp",
        "--reset_password",
        dest="reset_password",
        action="store_true",
        help="Reset Password of users"
    )

    args = parser.parse_args()

    return args
