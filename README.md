# Keycloak User Creation

This script is used to create multiple users for keycloak at once

## Usage

```
usage: main.py [-h] -c USER_CSV -dp DEFAULT_PASSWORD -a USERNAME -p PASSWORD
               [-e ENCODING] -d DOMAIN -r REALM [-u] [-dl DELIMITER] [-ex]
               [-rp]

optional arguments:
  -h, --help            show this help message and exit
  -c USER_CSV, --csv USER_CSV
                        Path to user.csv (default: None)
  -dp DEFAULT_PASSWORD, --default_password DEFAULT_PASSWORD
                        Default Password to be set for the created users
                        (default: None)
  -a USERNAME, --admin USERNAME
                        Admin Username (default: None)
  -p PASSWORD, --pass PASSWORD
                        Admin Password (default: None)
  -e ENCODING, --encoding ENCODING
                        Set Encoding for input file (default: UTF-8)
  -d DOMAIN, --domain DOMAIN
                        Platform Domain (i.e. stage.beispiel-stadt.de)
                        (default: None)
  -r REALM, --realm REALM
                        Keycloak Realm Name (default: None)
  -u, --update_existing_users
                        Update Role if User exists already (default: False)
  -dl DELIMITER, --delimiter DELIMITER
                        Set Delimiter for roles in the USER_CSV file (default:
                        +)
  -ex, --export_only    Only generate a list of current users. (default:
                        False)
  -rp, --reset_password
                        Reset Password of users (default: False)
```


> **Note:** The default password needs to comply to installed password policies.


## Format of the input csv file

```
Vorname,Name,E-Mail
Thomas,Haarhoff,thomas@haarhoff.de
Döäüß,Nâáà-Pu,Dö.ä-üß@Nâáà-Pu
```

### Header
The header contains at least 3 fields:
- Vorname: Specifying the first name of the User
- Nachname: Specifying the last name
- email: the email address for the user

Optionally you can add further fields. The name of the fields depends on clients in your instance. If you added Grafana to your instance, you might have a client called "grafana", and you want your user to be a grafana Admin. Your header will look like this:
```
Vorname,Name,E-Mail,grafana
Thomas,Haarhoff,thomas@haarhoff.de,grafanaAdmin
```

Additionally you can use realm roles by specifying ```_REALM``` in the header:
```
Vorname,Name,E-Mail,grafana,_REALM
Thomas,Haarhoff,thomas@haarhoff.de,grafanaAdmin,uma_authorization
```

You can even add multiple roles at once by separating them with a delimiter
```
Vorname,Name,E-Mail,grafana,_REALM
Thomas,Haarhoff,thomas@haarhoff.de,grafanaAdmin+grafanaEditor,uma_authorization+offline_access
```

If you dont want a User to have a role for a specific client, you can use "NO_ROLE" as role-name.
```
Vorname,Name,E-Mail,grafana
Thomas,Haarhoff,thomas@haarhoff.de,NO_ROLE
```

To add a User to a group, you can specify ```_GROUPS```in the header:
```
Vorname,Name,E-Mail,grafana,_GROUPS
Thomas,Haarhoff,thomas@haarhoff.de,grafanaAdmin+grafanaEditor,default_dataspace+your_group
```

If you dont want a User to belong to a group, you can use "NO_ROLE" as group-name.
```
Vorname,Name,E-Mail,grafana,_GROUPS
Thomas,Haarhoff,thomas@haarhoff.de,grafanaAdmin+grafanaEditor,NO_ROLE
```


## Outlook

- [ ] Coloums (clients) to give roles to users, if needed
- [ ] Additional account customization
- [ ] User update extension
