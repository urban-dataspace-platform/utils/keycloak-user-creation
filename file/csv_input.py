"""
csv_input.py

Description:
    This module provides functions for handling CSV input in the project. It includes
    functionalities such as reading CSV files, processing data, and other related tasks.

"""

import csv
import sys


def read_input_csv(user_csv, encoding):
    """
    Read user data from a CSV file.

    Parameters:
    - user_csv (str): The path to the CSV file containing user data.
    - encoding (str): The encoding format of the CSV file.

    Returns:
    list: A list of lists representing the user data read from the CSV file. Each inner list corresponds to a row.

    Raises:
    FileNotFoundError: If the specified CSV file is not found.
    UnicodeDecodeError: If there is an issue decoding the file using the provided encoding.
    csv.Error: If there is an issue reading the CSV file.

    Note:
    - The CSV file is expected to have columns in the following order: 'Vorname', 'Name', 'E-Mail'.
    - Columns after these 3 have to be either _GROUPS, _REALM, or <client_name>
    - The function performs a check to ensure that the header row matches the expected format. If not, it prints
      an error message and exits the program.
    """
    with open(user_csv, newline='', encoding=encoding) as csvfile:
        reader = csv.reader(csvfile)
        users = []
        for row in reader:
            users.append(row)
        header = users[0]
        if header[0] != 'Vorname' or header[1] != 'Name' or header[2] != 'E-Mail':
            print("File not in expected format")
            sys.exit(0)
        return users
