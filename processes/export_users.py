"""
export_users.py

Description:
    This module contains functions for exporting user data as part of the project's
    export process. It includes functionalities for data retrieval, formatting, and export.

"""

import json
import requests

from api import idm_requests


def print_current_user_stats(args):
    """
    Print statistics of current users in a Keycloak realm.

    Parameters:
    - args (argparse.Namespace): Parsed command-line arguments.

    Returns:
    int: Exit code (0 for success).

    Note:
    - This function authenticates the admin user and retrieves user statistics.
    - It sends GET requests to the Keycloak API to fetch user information, including groups and roles.
    - The URL is constructed based on the provided domain and realm.
    - The request includes the authorization token in the headers.
    - The function prints user statistics, including User ID, Username, First Name, Last Name, Account Usage,
      Groups, and Roles.
    - For each user, it checks for unused accounts and compiles group and role information.
    - The function returns 0 for success.
    """
    token = idm_requests.admin_login(args.username, args.password, args.domain)

    # Get Users
    # TODO move to idm requests
    url = "https://idm." + args.domain + "/auth/admin/realms/" + args.realm + "/users"
    headers = {
        "Accept": "application/json",
        "Authorization": "Bearer " + token
    }
    data = {}

    result = requests.get(url+"?max=1000", headers=headers, json=data, timeout=10)
    myjson = json.loads(result.content)

    print("UserID,Username,Vorname,Name,Genutzt,Groups,Roles")
    for user in myjson:
        unused_account = False
        groupstring = ""
        rolestring = "NONE"

        userurl = url+"/"+user["id"]
        groupurl = userurl+"/groups"
        groupresult = requests.get(groupurl, headers=headers, json=data, timeout=10)
        groupjson = json.loads(groupresult.content)

        roleurl = userurl + "/role-mappings"
        roleresult = requests.get(roleurl, headers=headers, json=data, timeout=10)
        rolejson = json.loads(roleresult.content)

        if not user["requiredActions"]:
            unused_account = True
        for group in groupjson:
            groupstring = group["path"][1:] + "+" + groupstring

        if "clientMappings" in rolejson:
            rolestring = ""
            for role in rolejson["clientMappings"]:
                rolestring = rolestring + rolejson["clientMappings"][role]["client"]+":"
                for mapping in rolejson["clientMappings"][role]["mappings"]:
                    rolestring = rolestring+mapping["name"]+"+"
                rolestring = rolestring[:-1] + " "
        print(user["id"] + "," +
              user["username"] + "," +
              user["firstName"] + "," +
              user["lastName"] + "," +
              ("Ja" if unused_account else "Nein") + "," +
              groupstring[:-1] + "," +
              rolestring)
        # TODO Relogin after 10
    return 0
