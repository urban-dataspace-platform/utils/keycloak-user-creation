"""
update_users.py

Description:
    This module includes functions for updating user information as part of the project's
    update process. It encompasses functionalities for creation and update idm related data.

"""
import sys

from file import csv_input
from api import idm_requests


def update_users(args):
    """
    Update user information in a Keycloak realm based on the provided CSV file.

    Parameters:
    - args (argparse.Namespace): Parsed command-line arguments.

    Note:
    - This function reads user information from a CSV file and updates user roles in the Keycloak realm.
    - It authenticates the admin user, retrieves client, role, and group information, and checks referenced clients.
    - For each user, it checks the number of roles assigned and builds a list of roles to be applied.
    - The roles are fetched from the realm and client roles, and if a role is invalid, the function exits.
    - It then calls the `create_user` function from `idm_requests` to create or update the user with specified roles.
    - The function refreshes the admin token every 10 users to avoid token expiry.
    """
    users = csv_input.read_input_csv(args.user_csv, args.encoding)
    token = idm_requests.admin_login(args.username, args.password, args.domain)

    update_existing_users = args.update_existing_users
    count = 0

    clients = users[0][3:]
    users.pop(0)

    # idm_requests.test(args.domain, args.realm, token)
    roles_info = idm_requests.get_client_roles(args.domain, args.realm, token, args.username)
    roles_realm_info = idm_requests.get_realm_roles(args.domain, args.realm, token)
    groups_info = idm_requests.get_groups(args.domain, args.realm, token)

    # check if clients exist
    for client in clients:
        client_valid = False
        if client in ("_REALM", "_GROUPS"):
            client_valid = True
        else:
            for role in roles_info:
                if client == role[0]:
                    client_valid = True
                    break

        if not client_valid:
            print("Invalid client \"" + client + "\"")
            sys.exit(0)

    for user in users:
        # check amount of roles per user
        if len(user) != len(clients) + 3:
            print("User \"" + user[2] + "\" does not have the same amount of roles assigned as definded in the header")
            sys.exit(0)

        user_roles = user[3:]
        roles_to_apply = []

        # copy ids to roles_to_apply
        for i, current_client in enumerate(clients):
            if current_client in ("_REALM", "_GROUPS"):
                for client in roles_info:
                    if current_client == client[0]:
                        role_array = user_roles[i].split(args.delimiter)

                        for role in role_array:
                            role_found = False

                            for client_role in client[2]:
                                if role == client_role[0]:
                                    roles_to_apply_tmp = []
                                    roles_to_apply_tmp.append(client[1])
                                    roles_to_apply_tmp.append(client_role[1])
                                    roles_to_apply_tmp.append(False)
                                    roles_to_apply_tmp.append(role)
                                    roles_to_apply.append(roles_to_apply_tmp)
                                    role_found = True
                                    break

                            for client_role in client[3]:
                                if role == client_role[0]:
                                    roles_to_apply_tmp = []
                                    roles_to_apply_tmp.append(client[1])
                                    roles_to_apply_tmp.append(client_role[1])
                                    roles_to_apply_tmp.append(False)
                                    roles_to_apply_tmp.append(role)
                                    roles_to_apply.append(roles_to_apply_tmp)
                                    role_found = True
                                    break

                            if not role_found and role != "NO_ROLE":
                                print("User \"" + user[2] + "\" contains invalid role \"" + role + "\"!")
                                sys.exit(0)
            elif current_client == "_REALM":
                for role in roles_realm_info:
                    user_role_array = user_roles[i].split(args.delimiter)
                    for user_role in user_role_array:
                        if user_role == role[0]:
                            roles_to_apply.append(["_REALM", role[1]])
            elif current_client == "_GROUPS":
                for group in groups_info:
                    user_role_array = user_roles[i].split(args.delimiter)
                    for user_role in user_role_array:
                        if user_role == group[0]:
                            roles_to_apply.append(["_GROUP", group[1]])
        # TODO split update and create
        idm_requests.create_user(args.domain, args.realm, token, user[0], user[1], user[2], args.default_password,
                                 roles_to_apply, update_existing_users)
        count += 1
        # We refresh the admin token each 10 users, to be sure not to run in token expiry
        if count == 10:
            token = idm_requests.admin_login(args.username, args.password, args.domain)
            count = 0
