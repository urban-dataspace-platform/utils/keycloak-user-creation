"""
reset_password.py

Description:
    This module provides functions for handling user password reset functionality. It will
    reset the password to a temporary password set by the -dp parameter.

"""

import requests

from file import csv_input
from api import idm_requests


def reset_passwords(args):
    """
    Reset passwords for users in a Keycloak realm.

    Parameters:
    - args (argparse.Namespace): Parsed command-line arguments.

    Note:
    - This function reads user information from a CSV file and resets the passwords for each user.
    - It authenticates the admin user, retrieves user IDs, and sends PUT requests to reset passwords.
    - The URL is constructed based on the provided domain and realm.
    - The request includes the authorization token and a JSON payload with the new temporary password.
    - The function prints the result of each password reset.
    - TODO: Re-login after processing a certain number of users.
    """
    users = csv_input.read_input_csv(args.user_csv, args.encoding)
    token = idm_requests.admin_login(args.username, args.password, args.domain)
    users.pop(0)
    for user in users:
        # TODO move to idm requests
        user_id = idm_requests.get_user_id(args.domain, args.realm, token, user[2])
        url = "https://idm." + args.domain + "/auth/admin/realms/" + args.realm + "/users/"+user_id
        headers = {
            "Accept": "application/json",
            "Authorization": "Bearer " + token,
            "Content-Type": "application/json"
        }
        data = { "type": "password", "temporary": True, "value": args.default_password }

        result = requests.put(url+"/reset-password", headers=headers, json=data, timeout=10)
        print(result)
    # TODO relogin after 10
