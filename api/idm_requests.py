"""
idm_requests.py

Description:
    This module contains functions for making requests to a Keycloak Identity Management (IDM) API.
    It includes functionalities for interacting with the API, handling responses, and other tasks.

"""

import sys
import json
import requests


def admin_login(user, password, domain):
    """
    Authenticate an admin user and retrieve an access token.

    Parameters:
    - user (str): Admin username for authentication.
    - password (str): Admin password for authentication.
    - domain (str): Platform domain for constructing the authentication URL.

    Returns:
    str: Access token obtained upon successful admin login.

    Note:
    - This function performs an admin login by sending a POST request to the Keycloak authentication endpoint.
    - The URL is constructed based on the provided domain.
    - The admin credentials (username and password) are included in the request payload.
    - If the login is successful (HTTP status code 200), the access token is extracted from the response JSON.
    - If the login fails, an error message is printed, and the program exits.
    """
    url = "https://idm." + domain + "/auth/realms/master/protocol/openid-connect/token"
    data = {
        "grant_type": "password",
        "client_id": "admin-cli",
        "username": user,
        "password": password
    }
    result = requests.post(url, data=data, timeout=10)
    if result.status_code != 200:
        print("Admin Login failed. Statuscode != 200")
        sys.exit(0)
    token = result.json()['access_token']
    # print(json.dumps(result.json(), indent=2))

    return token


def get_realm_roles(domain, realm, token):
    """
    Fetch the list of realm roles for a given Keycloak realm.

    Parameters:
    - domain (str): Platform domain for constructing the Keycloak API URL.
    - realm (str): Keycloak Realm Name.
    - token (str): Access token obtained from admin login for authorization.

    Returns:
    list: A list of lists containing role information. Each inner list includes the role name and ID.

    Note:
    - This function sends a GET request to the Keycloak API to retrieve the list of roles for the specified realm.
    - The URL is constructed based on the provided domain and realm.
    - The request includes the authorization token in the headers.
    - If the request is successful (HTTP status code 200), the role information is extracted from the response JSON.
    - If the request fails, an error message is printed, and the program exits.
    """
    url = "https://idm." + domain + "/auth/admin/realms/" + realm + "/roles"
    headers = {
        "Accept": "application/json",
        "Authorization": "Bearer " + token
    }
    data = {}

    result = requests.get(url, headers=headers, json=data, timeout=10)
    if result.status_code != 200:
        print("Failed to fetch list of Roles.")
        sys.exit(0)

    roles = []

    for role in json.loads(result.content):
        role_info = []
        role_info.append(role["name"])
        role_info.append(role["id"])
        roles.append(role_info)

    return roles


def set_realm_role(domain, realm, token, role_id, user_id):
    """
    Assign a realm role to a user in a Keycloak realm.

    Parameters:
    - domain (str): Platform domain for constructing the Keycloak API URL.
    - realm (str): Keycloak Realm Name.
    - token (str): Access token obtained from admin login for authorization.
    - role_id (str): ID of the realm role to be assigned.
    - user_id (str): ID of the user to whom the role will be assigned.

    Note:
    - This function sends a POST request to the Keycloak API to assign a realm role to a user.
    - The URL is constructed based on the provided domain, realm, and user ID.
    - The request includes the authorization token in the headers.
    - The role information is specified in the request payload as a JSON array.
    - The function does not return any value; it is assumed that the operation is successful if no exception is raised.
    """
    url = "https://idm." + domain + "/auth/admin/realms/" + realm + "/users/" + user_id + "/role-mappings/realm"
    headers = {
        "Accept": "application/json",
        "Authorization": "Bearer " + token
    }
    data = [
        {"id": role_id, "name": "uma_authorization", "composite": False, "clientRole": False, "containerId": "Merch"}]

    requests.post(url, headers=headers, json=data, timeout=10)


def get_clients(domain, realm, token):
    """
    Fetch the list of clients in a Keycloak realm.

    Parameters:
    - domain (str): Platform domain for constructing the Keycloak API URL.
    - realm (str): Keycloak Realm Name.
    - token (str): Access token obtained from admin login for authorization.

    Returns:
    list: A list of lists containing client information. Each inner list includes the client name and ID.

    Note:
    - This function sends a GET request to the Keycloak API to retrieve the list of clients for the specified realm.
    - The URL is constructed based on the provided domain and realm.
    - The request includes the authorization token in the headers.
    - If the request is successful (HTTP status code 200), the client information is extracted from the response JSON.
    - If the request fails, an error message is printed, and the program exits.
    """
    url = "https://idm." + domain + "/auth/admin/realms/" + realm + "/clients"
    headers = {
        "Accept": "application/json",
        "Authorization": "Bearer " + token
    }
    data = {}

    result = requests.get(url, headers=headers, json=data, timeout=10)
    if result.status_code != 200:
        print("Failed to fetch list of clients.")
        sys.exit(0)

    clients = []

    for client in json.loads(result.content):
        client_info = []
        client_info.append(client["clientId"])
        client_info.append(client["id"])
        clients.append(client_info)

    return clients


def get_client_roles(domain, realm, token, admin_email):
    """
    Fetch the roles assigned to an admin user for each client in a Keycloak realm.

    Parameters:
    - domain (str): Platform domain for constructing the Keycloak API URL.
    - realm (str): Keycloak Realm Name.
    - token (str): Access token obtained from admin login for authorization.
    - admin_email (str): Admin user email to fetch roles for.

    Note:
    - This function iterates over all clients in a realm and retrieves roles assigned to the admin user for each client.
    - It uses the `get_clients` and `get_user_id` functions to gather necessary information.
    - For each client, a GET request is sent to the Keycloak API to fetch available roles for the admin user.
    - The URL is constructed based on the provided domain, realm, admin user ID, and client ID.
    - The request includes the authorization token in the headers.
    - If the request is successful (HTTP status code 200), the role information is extracted from the response JSON.
    - If the request fails, an error message is printed, and the program exits.
    - The roles are appended to the respective client information.
    """
    clients = get_clients(domain, realm, token)
    admin_user_id = get_user_id(domain, realm, token, admin_email)

    for client in clients:
        url = (f"https://idm."
               f"{domain}/auth/admin/realms/"
               f"{realm}/users/"
               f"{admin_user_id}/role-mappings/clients/"
               f"{client[1]}/available")

        headers = {
            "Accept": "application/json",
            "Authorization": "Bearer " + token
        }
        data = {}

        result = requests.get(url, headers=headers, json=data, timeout=10)
        if result.status_code != 200:
            print("Failed to fetch list of Roles.")
            sys.exit(0)

        roles = []

        for role in json.loads(result.content):
            role_info = []
            role_info.append(role["name"])
            role_info.append(role["id"])
            roles.append(role_info)

        client.append(roles)

    for client in clients:
        url = (f"https://idm."
               f"{domain}/auth/admin/realms/"
               f"{realm}/users/"
               f"{admin_user_id}/role-mappings/clients/"
               f"{client[1]}/composite")

        headers = {
            "Accept": "application/json",
            "Authorization": "Bearer " + token
        }
        data = {}

        result = requests.get(url, headers=headers, json=data, timeout=10)
        if result.status_code != 200:
            print("Failed to fetch list of Roles.")
            sys.exit(0)

        roles = []

        for role in json.loads(result.content):
            role_info = []
            role_info.append(role["name"])
            role_info.append(role["id"])
            roles.append(role_info)

        client.append(roles)

    return clients


def get_groups(domain, realm, token):
    """
    Fetch the list of groups in a Keycloak realm.

    Parameters:
    - domain (str): Platform domain for constructing the Keycloak API URL.
    - realm (str): Keycloak Realm Name.
    - token (str): Access token obtained from admin login for authorization.

    Returns:
    list: A list of lists containing group information. Each inner list includes the group name and ID.

    Note:
    - This function sends a GET request to the Keycloak API to retrieve the list of groups for the specified realm.
    - The URL is constructed based on the provided domain and realm.
    - The request includes the authorization token in the headers.
    - If the request is successful (HTTP status code 200), the group information is extracted from the response JSON.
    - If the request fails, an error message is printed, and the program exits.
    """
    url = "https://idm." + domain + "/auth/admin/realms/" + realm + "/groups"
    headers = {
        "Accept": "application/json",
        "Authorization": "Bearer " + token
    }
    data = {}

    result = requests.get(url, headers=headers, json=data, timeout=10)
    if result.status_code != 200:
        print("Failed to fetch list of groups.")
        sys.exit(0)

    groups = []

    for group in json.loads(result.content):
        group_info = []
        group_info.append(group["name"])
        group_info.append(group["id"])
        groups.append(group_info)

    return groups


def set_role(domain, realm, token, user_id, role):
    """
    Assign a client role to a user in a Keycloak realm.

    Parameters:
    - domain (str): Platform domain for constructing the Keycloak API URL.
    - realm (str): Keycloak Realm Name.
    - token (str): Access token obtained from admin login for authorization.
    - user_id (str): ID of the user to whom the role will be assigned.
    - role (list): A list containing role information - [client_id, role_id, composite, role_name].

    Note:
    - This function sends a POST request to the Keycloak API to assign a client role to a user.
    - The URL is constructed based on the provided domain, realm, user ID, and client ID.
    - The request includes the authorization token in the headers.
    - The role information is specified in the request payload as a JSON array.
    - If the request is unsuccessful (HTTP status code other than 204), an error message is printed.
    """
    url = (f"https://idm."
           f"{domain}/auth/admin/realms/"
           f"{realm}/users/"
           f"{user_id}/role-mappings/clients/"
           f"{role[0]}")
    headers = {
        "Accept": "application/json",
        "Authorization": "Bearer " + token
    }
    data = [{"id": role[1], "name": role[3], "composite": role[2], "clientRole": True, "containerId": role[0]}]

    result = requests.post(url, headers=headers, json=data, timeout=10)
    if result.status_code != 204:
        print("Error occured when trying to apply client role")


def set_group(domain, realm, token, user_id, group):
    """
    Assign a user to a group in a Keycloak realm.

    Parameters:
    - domain (str): Platform domain for constructing the Keycloak API URL.
    - realm (str): Keycloak Realm Name.
    - token (str): Access token obtained from admin login for authorization.
    - user_id (str): ID of the user to whom the group will be assigned.
    - group (list): A list containing group information - [group_id, group_name].

    Note:
    - This function sends a PUT request to the Keycloak API to assign a user to a group.
    - The URL is constructed based on the provided domain, realm, user ID, and group ID.
    - The request includes the authorization token in the headers.
    - The group information is specified in the request payload as a JSON array.
    - If the request is unsuccessful (HTTP status code other than 204), an error message is printed.
    """
    url = (f"https://idm."
           f"{domain}/auth/admin/realms/"
           f"{realm}/users/"
           f"{user_id}/groups/"
           f"{group}")
    headers = {
        "Accept": "application/json",
        "Authorization": "Bearer " + token
    }
    data = [{"groupId": group[1], "realm": realm, "userId": user_id}]

    result = requests.put(url, headers=headers, json=data, timeout=10)
    if result.status_code != 204:
        print("Error occured when trying to apply group")


def get_user_id(domain, realm, token, email):
    """
    Get the user ID associated with a given email address in a Keycloak realm.

    Parameters:
    - domain (str): Platform domain for constructing the Keycloak API URL.
    - realm (str): Keycloak Realm Name.
    - token (str): Access token obtained from admin login for authorization.
    - email (str): Email address for which to retrieve the user ID.

    Returns:
    str: User ID associated with the specified email address.

    Note:
    - This function sends a GET request to the Keycloak API to retrieve user information based on the provided email.
    - The URL is constructed based on the provided domain, realm, and email.
    - The request includes the authorization token in the headers.
    - The function returns the user ID extracted from the API response.
    """
    url = "https://idm." + domain + "/auth/admin/realms/" + realm + "/users?username=" + email
    headers = {
        "Accept": "application/json",
        "Authorization": "Bearer " + token
    }
    data = {}

    result = requests.get(url, headers=headers, json=data, timeout=10)
    return json.loads(result.content)[0]["id"]


def create_user(domain, realm, token, firstname, lastname, email, default_pass, roles, update_existing_users):
    """
    Create a new user in a Keycloak realm and assign specified roles.

    Parameters:
    - domain (str): Platform domain for constructing the Keycloak API URL.
    - realm (str): Keycloak Realm Name.
    - token (str): Access token obtained from admin login for authorization.
    - firstname (str): First name of the user.
    - lastname (str): Last name of the user.
    - email (str): Email address of the user.
    - default_pass (str): Default password to be set for the user.
    - roles (list): A list of roles to be assigned to the user.
    - update_existing_users (bool): If True, update roles for existing users with the same email.

    Note:
    - This function sends a POST request to the Keycloak API to create a new user with the provided information.
    - The URL is constructed based on the provided domain and realm.
    - The request includes the authorization token in the headers and user information in the request payload.
    - After creating the user, it retrieves the user ID using the `get_user_id` function.
    - If the user is created successfully (HTTP status 201), it prints a success message and assigns specified roles.
    - If the user already exists (HTTP status 409), it checks if `update_existing_users` is True and updates the user.
    - If there is an authentication error (HTTP status 401), it prints an error message and exits the program.
    - For other unknown errors, it prints an error status and exits the program.
    """
    url = "https://idm." + domain + "/auth/admin/realms/" + realm + "/users"
    headers = {
        "Accept": "application/json",
        "Authorization": "Bearer " + token
    }
    data = {
        "username": email,
        "enabled": True,
        "totp": False,
        "emailVerified": True,
        "firstName": firstname,
        "lastName": lastname,
        "email": email,
        "disableableCredentialTypes": [],
        "requiredActions": [
            "UPDATE_PASSWORD"
        ],
        "notBefore": 0,
        "access": {
            "manageGroupMembership": False,
            "view": False,
            "mapRoles": False,
            "impersonate": False,
            "manage": False
        },
        "credentials": [{
            "type": "password",
            "value": default_pass,
            "temporary": True
        }]

    }
    result = requests.post(url, headers=headers, json=data, timeout=10)
    user_id = get_user_id(domain, realm, token, email)

    if result.status_code == 201:
        print("[Created New User    ]: " + firstname + " " + lastname)

        for i in roles:
            if i[0] != "_REALM" and i[0] != "_GROUP":
                set_role(domain, realm, token, user_id, i)
            else:
                if i[0] == "_REALM":
                    set_realm_role(domain, realm, token, i[1], user_id)
                elif i[0] == "_GROUP":
                    set_group(domain, realm, token, user_id, i[1])

    elif result.status_code == 409:
        print("[User Already Exist  ]: " + firstname + " " + lastname)

        if update_existing_users:
            for i in roles:
                if i[0] != "_REALM" and i[0] != "_GROUP":
                    set_role(domain, realm, token, user_id, i)
                else:
                    if i[0] == "_REALM":
                        set_realm_role(domain, realm, token, i[1], user_id)
                    elif i[0] == "_GROUP":
                        set_group(domain, realm, token, user_id, i[1])

    elif result.status_code == 401:
        print("[Authentication Error]: " + firstname + " " + lastname)
        print(result.json())
        sys.exit(0)
    else:
        print("[Unknown Error Status]: " + str(result.status_code))
        print(result.json())
        sys.exit(0)
